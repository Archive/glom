# Miroslav Nikolić <miroslavnikolic@rocketmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: Small Business Example\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?\n"
"product=glom&keywords=I18N+L10N&component=general\n"
"PO-Revision-Date: 2014-01-27 21:52+0200\n"
"Last-Translator: Miroslav Nikolić <miroslavnikolic@rocketmail.com>\n"
"Language-Team: Serbian <gnom@prevod.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : "
"n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

msgctxt "Database Title"
msgid "Small Business Example"
msgstr "Primer sitne radinosti"

msgctxt "Table (contacts)"
msgid "Contacts"
msgstr "Kontakti"

msgctxt "Field (contact_id). Parent table: contacts"
msgid "Contact ID"
msgstr "IB kontakta"

msgctxt "Field (name_first). Parent table: contacts"
msgid "First Name"
msgstr "Ime"

msgctxt "Field (name_middle). Parent table: contacts"
msgid "Middle Name"
msgstr "Ime oca"

msgctxt "Field (name_last). Parent table: contacts"
msgid "Last Name"
msgstr "Prezime"

msgctxt "Field (name_title). Parent table: contacts"
msgid "Title"
msgstr "Zvanje"

msgctxt "Field Choice. Parent table: contacts, Parent Field: name_title"
msgid "Mr"
msgstr "G-din"

msgctxt "Field Choice. Parent table: contacts, Parent Field: name_title"
msgid "Mrs"
msgstr "G-đa"

msgctxt "Field Choice. Parent table: contacts, Parent Field: name_title"
msgid "Ms"
msgstr "G-din"

msgctxt "Field Choice. Parent table: contacts, Parent Field: name_title"
msgid "Miss"
msgstr "G-đica"

msgctxt "Field Choice. Parent table: contacts, Parent Field: name_title"
msgid "Dr"
msgstr "Dr."

msgctxt "Field Choice. Parent table: contacts, Parent Field: name_title"
msgid "Prof"
msgstr "Prof."

msgctxt "Field (address_street). Parent table: contacts"
msgid "Street"
msgstr "Ulica"

msgctxt "Field (address_town). Parent table: contacts"
msgid "Town"
msgstr "Grad"

msgctxt "Field (address_state). Parent table: contacts"
msgid "State"
msgstr "Republika"

msgctxt "Field (address_country). Parent table: contacts"
msgid "Country"
msgstr "Država"

msgctxt "Field (address_postcode). Parent table: contacts"
msgid "Postcode"
msgstr "Poštanski broj"

msgctxt "Field (date_of_birth). Parent table: contacts"
msgid "Date of Birth"
msgstr "Datum rođenja"

msgctxt "Field (comments). Parent table: contacts"
msgid "Comments"
msgstr "Napomene"

msgctxt "Field (name_full). Parent table: contacts"
msgid "Full Name"
msgstr "Ime i prezime"

msgctxt "Field (picture). Parent table: contacts"
msgid "Picture"
msgstr "Slika"

msgctxt "Field (email). Parent table: contacts"
msgid "Email Address"
msgstr "Adresa el. pošte"

msgctxt "Field (website). Parent table: contacts"
msgid "Web Site"
msgstr "Veb stranica"

msgctxt "Field (tel_home). Parent table: contacts"
msgid "Home Telephone"
msgstr "Kućni telefon"

msgctxt "Field (tel_work). Parent table: contacts"
msgid "Work Telephone"
msgstr "Poslovni telefon"

msgctxt "Field (tel_mobile). Parent table: contacts"
msgid "Mobile Telephone"
msgstr "Mobilni telefon"

msgctxt "Field (tel_fax). Parent table: contacts"
msgid "Fax"
msgstr "Faks"

msgctxt "Report (by_country). Parent table: contacts"
msgid "Contacts By Country"
msgstr "Kontakata po državi"

msgctxt "Report (by_country_by_town). Parent table: contacts"
msgid "By Country, By Town"
msgstr "Prema državi, prema gradu"

msgctxt "Print Layout (contact_details). Parent table: contacts"
msgid "Contact Details"
msgstr "Detalji kontakta"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Contacts"
msgstr "Kontakti"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Overview"
msgstr "Pregled"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Contact ID:"
msgstr "IB kontakta:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Full Name:"
msgstr "Ime i prezime:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Name"
msgstr "Ime"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Title:"
msgstr "Naziv:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "First Name:"
msgstr "Ime:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Middle Name:"
msgstr "Ime oca:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Last Name:"
msgstr "Prezime:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Address"
msgstr "Adresa"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Street:"
msgstr "Ulica:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Town:"
msgstr "Grad:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "State:"
msgstr "Republika:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Postcode:"
msgstr "Poštanski broj:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Country:"
msgstr "Država:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Telephone"
msgstr "Telefon"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Work Telephone:"
msgstr "Poslovni telefon:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Mobile Telephone:"
msgstr "Mobilni telefon:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Home Telephone:"
msgstr "Kućni telefon:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Fax:"
msgstr "Faks:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Date of Birth:"
msgstr "Datum rođenja:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Picture:"
msgstr "Slika:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Comments:"
msgstr "Napomene:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Web Site:"
msgstr "Veb stranica:"

msgctxt "Text. Parent table: contacts, Print Layout: contact_details"
msgid "Email Address:"
msgstr "Adresa el. pošte:"

msgctxt "Layout Group (overview). Parent table: contacts"
msgid "Overview"
msgstr "Pregled"

msgctxt "Layout Group (name). Parent table: contacts, Parent Group: details"
msgid "Name"
msgstr "Ime"

msgctxt "Layout Group (address). Parent table: contacts, Parent Group: details"
msgid "Address"
msgstr "Adresa"

msgctxt ""
"Layout Group (telephone). Parent table: contacts, Parent Group: details"
msgid "Telephone"
msgstr "Telefon"

msgctxt "Button. Parent table: contacts, Parent Group: details"
msgid "Test Button"
msgstr "Dugme probe"

msgctxt "Field (product_name). Parent table: invoice_lines"
msgid "Product Name"
msgstr "Naziv proizvoda"

msgctxt "Field (product_id). Parent table: invoice_lines"
msgid "Product Id"
msgstr "IB proizvoda"

msgctxt "Field (product_price). Parent table: invoice_lines"
msgid "Product Price"
msgstr "Cena proizvoda"

msgctxt "Field (invoice_id). Parent table: invoice_lines"
msgid "Invoice ID"
msgstr "IB računa"

msgctxt "Field (count). Parent table: invoice_lines"
msgid "Number Of"
msgstr "Broj"

msgctxt "Field (total_price). Parent table: invoice_lines"
msgid "Total Price"
msgstr "Ukupna cena"

msgctxt "Field (vat_percentage). Parent table: invoice_lines"
msgid "VAT Percentage"
msgstr "VAT postotak"

msgctxt "Field (total_price_vat). Parent table: invoice_lines"
msgid "Total VAT"
msgstr "Ukupni VAT"

msgctxt "Relationship (products). Parent table: invoice_lines"
msgid "Products"
msgstr "Proizvodi"

msgctxt "Table (invoices)"
msgid "Invoices"
msgstr "Računi"

msgctxt "Field (invoice_id). Parent table: invoices"
msgid "Invoice ID"
msgstr "IB računa"

msgctxt "Field (contact_id). Parent table: invoices"
msgid "Contact ID"
msgstr "IB kontakta"

msgctxt "Field (date). Parent table: invoices"
msgid "Date"
msgstr "Datum"

msgctxt "Field (comment). Parent table: invoices"
msgid "Comment"
msgstr "Napomena"

msgctxt "Field (price_total). Parent table: invoices"
msgid "Total Price"
msgstr "Ukupna cena"

msgctxt "Field (vat_total). Parent table: invoices"
msgid "Total VAT"
msgstr "Ukupni VAT"

msgctxt "Field (price_total_with_vat). Parent table: invoices"
msgid "Total Price With Vat"
msgstr "Ukupna cena sa Vatom"

msgctxt "Relationship (invoice_lines). Parent table: invoices"
msgid "Invoice Lines"
msgstr "Redovi računa"

msgctxt "Relationship (contacts). Parent table: invoices"
msgid "Contacts"
msgstr "Kontakti"

msgctxt "Report (by_customer). Parent table: invoices"
msgid "By Customer"
msgstr "Prema potrošaču"

msgctxt "Layout Group (overview). Parent table: invoices"
msgid "Overview"
msgstr "Pregled"

msgctxt ""
"Layout Group (customer). Parent table: invoices, Parent Group: details"
msgid "Customer"
msgstr "Potrošač"

msgctxt "Table (products)"
msgid "Products"
msgstr "Proizvodi"

msgctxt "Field (product_id). Parent table: products"
msgid "Product ID"
msgstr "IB proizvoda"

msgctxt "Field (comment). Parent table: products"
msgid "Comment"
msgstr "Napomena"

msgctxt "Field (description). Parent table: products"
msgid "Description"
msgstr "Opis"

msgctxt "Field (name). Parent table: products"
msgid "Name"
msgstr "Naziv"

msgctxt "Field (price). Parent table: products"
msgid "Price"
msgstr "Cena"

msgctxt "Field (vat_percent). Parent table: products"
msgid "Vat Percent"
msgstr "Vat postotak"

msgctxt "Layout Group (overview). Parent table: products"
msgid "Overview"
msgstr "Pregled"

msgctxt "Layout Group (details). Parent table: products"
msgid "Details"
msgstr "Podaci"

msgctxt "Table (staff)"
msgid "Staff"
msgstr "Osoblje"

msgctxt "Field (staff_id). Parent table: staff"
msgid "Staff ID"
msgstr "IB osoblja"

msgctxt "Field (name_title). Parent table: staff"
msgid "Title"
msgstr "Zvanje"

msgctxt "Field Choice. Parent table: staff, Parent Field: name_title"
msgid "Mr"
msgstr "G-din"

msgctxt "Field Choice. Parent table: staff, Parent Field: name_title"
msgid "Mrs"
msgstr "G-đa"

msgctxt "Field Choice. Parent table: staff, Parent Field: name_title"
msgid "Miss"
msgstr "G-đica"

msgctxt "Field Choice. Parent table: staff, Parent Field: name_title"
msgid "Dr"
msgstr "Dr."

msgctxt "Field Choice. Parent table: staff, Parent Field: name_title"
msgid "Prof"
msgstr "Prof."

msgctxt "Field (name_first). Parent table: staff"
msgid "First Name"
msgstr "Ime"

msgctxt "Field (name_second). Parent table: staff"
msgid "Second Name"
msgstr "Drugo ime"

msgctxt "Field (name_last). Parent table: staff"
msgid "Last Name"
msgstr "Prezime"

msgctxt "Field (address_street). Parent table: staff"
msgid "Street"
msgstr "Ulica"

msgctxt "Field (address_town). Parent table: staff"
msgid "Town"
msgstr "Grad"

msgctxt "Field (address_state). Parent table: staff"
msgid "State"
msgstr "Republika"

msgctxt "Field (address_country). Parent table: staff"
msgid "Country"
msgstr "Država"

msgctxt "Field (address_postcode). Parent table: staff"
msgid "Postcode"
msgstr "Poštanski broj"

msgctxt "Field (date_of_birth). Parent table: staff"
msgid "Date Of Birth"
msgstr "Datum rođenja"

msgctxt "Field (position). Parent table: staff"
msgid "Position"
msgstr "Položaj"

msgctxt "Layout Group (overview). Parent table: staff"
msgid "Overview"
msgstr "Pregled"

msgctxt "Layout Group (name). Parent table: staff, Parent Group: details"
msgid "Name"
msgstr "Ime"

msgctxt "Layout Group (address). Parent table: staff, Parent Group: details"
msgid "Address"
msgstr "Adresa"
